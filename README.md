The entire application is written in Kotlin and supports a minimum API level of 26, Android 8.0. 
# Application functionalities
Based on the proposed solution in the thesis, the application functionalities are
* detect which machine we’re operating based on beacon ID
* show a selectable list of projects
* show a selectable list of materials
* show plank sizing data
* show plank edge banding data
* scan plank barcode
* communicate with tooz glasses API to send data for display in the glasses
* listen for touchpad events from the tooz glasses

# Libraries used

## Toozifier Library (tooz SDK)
The Toozifier Library provided by “tooz technologies” is used in the implementation in order to register to the glasses, send the data over to be displayed as graphic content, receive the single tap motion event and deregister when necessary. When registering to the Toozifier, a registration listener is specified. Based on registration/deregistration success or failure, the appropriate methods are executed.

## BLE Library
Bluetooth Low Energy (BLE) is used to detect Bluetooth Beacons because it has a lower power consumption. The library requires enabled Bluetooth and location permission to work. The BLE Scanner filters the devices based on the MAC addresses present in the “Machine” table in the database. No static beacon addresses are used in the code. To switch to another beacon for a machine, only the database needs to be updated. The scanner scans for devices every 10 seconds, sorts the found ones according to their RSSI (Received Signal Strength Indicator) and determines the closest beacon, i.e determines the closest machine. The application activities/fragments have Observers, monitoring for changes in the closest detected beacon.

## Room persistence Library
The Room persistence library is used to access the database robustly. There are entity classes that each represent a table in the database and each object of these entities represents a row in the table. Data Access Objects (DAO) are used to fetch data from the Room Database as instances of the entity objects and also update data. All the data in the application is live, meaning changes are updated in real-time. The only update in the database is the “done” field in the “Plank” table to indicate a finished procedure.

## Barcode Scanner
When using machines 2 and 3, we need individual plank data. To get that, there is a camera functionality in the bottom navigation menu of the application that scans the barcode attached to the plank when pointed at it. The application would then search the database for the plank with such a barcode value and return the row of data, which then gets displayed on the mobile device’s screen and in the glasses frame.